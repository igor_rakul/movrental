﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Web.Http;
using MovRental.Dtos;
using MovRental.Models;
using AutoMapper;
using System.Data.Entity;






namespace MovRental.Controllers.Api
{
    public class RentalsController : ApiController
    {
        ApplicationDbContext _context;

        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }
                
        [HttpGet]
        public IHttpActionResult GetAllRentals()
        {
            var rentalsQuery = _context.Rentals
                .Include(r => r.Customer)
                .Include(r => r.Movie);

            var rentalDtos = rentalsQuery
                .ToList()
                .Select(Mapper.Map<Rental,RentalDto>);
            
            return Ok(rentalDtos);
        }

        [HttpPost]
        public IHttpActionResult CreateRental(NewRentalDto rentalDto)
        {
            if (rentalDto.MovieIds.Count == 0)
                return BadRequest("No MoviesId have been provided.");

            // select customer
            var customer = _context.Customers.SingleOrDefault(c => c.Id == rentalDto.CustomerId);
            if (customer == null)
                return BadRequest("Customer Id is not valid.");

            // select movies
            var movies = _context.Movies.Where(m => rentalDto.MovieIds.Contains(m.Id)).ToList();


            if (movies.Count != rentalDto.MovieIds.Count)
                return BadRequest("One or more MoviesId are invalid.");

            foreach (var movie in movies)
            {
                if (movie.AmountAvailable == 0)
                    return BadRequest("Movie is not available");

                movie.AmountAvailable--;
                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                };

                _context.Rentals.Add(rental);
            }
            _context.SaveChanges();

            return Ok(HttpStatusCode.OK);
        }


        // api/rentals/18
        [HttpPut]
        //[Route("api/rentals/{id}")]
        public IHttpActionResult CloseRental(int id)
        {
            //return Ok(HttpStatusCode.OK);
            
            var rentalInDb = _context.Rentals
                .Include(r => r.Customer)
                .Include(r => r.Movie)
                .Single(r => r.Id == id);
            if (rentalInDb.DateReturned != null)
                return BadRequest();
            rentalInDb.DateReturned = DateTime.Now;
            rentalInDb.Movie.AmountAvailable++;
            _context.SaveChanges();

            return Ok(HttpStatusCode.OK);
        }

        

    }
}
