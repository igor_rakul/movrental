﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MovRental.Models;
using MovRental.ViewModels;
using MovRental.Extensions;

namespace MovRental.Controllers
{
    
    public class MoviesController : Controller
    {
        // create a movie context from db
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult New()
        {
            var genres = _context.Genres.ToList();
            var viewModel = new MovieFormViewModel
            {
                Genres = genres
            };
            return View("MovieForm", viewModel);
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            if (movie == null)
                return HttpNotFound();

                var viewModel = new MovieFormViewModel(movie)
                {
                    Genres = _context.Genres.ToList()
                };
           
            return View("MovieForm", viewModel);
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Delete(int id)
        {
            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            if (movie == null)
                return HttpNotFound();

            return RedirectToAction("ListMovies");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Save(Movie movie)
        {
            // check validation
            if (!ModelState.IsValid)
            {
                var viewModel = new MovieFormViewModel (movie)
                {
                    Genres = _context.Genres.ToList()
                };

                return View("MovieForm", viewModel);
            }
        
            if(movie.Id == 0)
            {
                movie.AmountAvailable = movie.AmountInStock;
                movie.DateAdded = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDb = _context.Movies.Single(m => m.Id == movie.Id);
                movieInDb.Name = movie.Name;
                movieInDb.ReleaseDate = movie.ReleaseDate;
                movieInDb.DateAdded = DateTime.Now;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.AmountInStock = movie.AmountInStock;
                
            }
            _context.SaveChanges();

            return RedirectToAction("", "Movies");
        }

        [Route("Movies/Details/{id}", Order = 1)]
        public ActionResult MovieDetailsShow(int id)
        {
            var selectedMovie = _context.Movies.Include(m => m.Genre).SingleOrDefault(c => c.Id == id);
            if (selectedMovie == null)
                return HttpNotFound();
            return View(selectedMovie);
        }

        //Show a list of movies
        [Route("Movies", Order = 2)]
        public ViewResult ListMovies()
        {
            // Create a movie list for request
            //var movies = _context.Movies.Include(m => m.Genre).ToList();
            // Reference a real movie list
            if (User.IsInRole(RoleName.CanManageMovies))
                return View("ListMovies");

            return View("ReadOnlyListMovies");
        }
        
        
        
    } 
}