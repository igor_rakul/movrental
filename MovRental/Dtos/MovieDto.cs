﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MovRental.Models;

namespace MovRental.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please, indicate a name of movie.")]
        public string Name { get; set; }

        
        [Required(ErrorMessage = "Please, indicate a release date.")]
        public DateTime? ReleaseDate { get; set; }

        [Required(ErrorMessage = "Please, indicate an amount in stock.")]
        [Range(1, 20, ErrorMessage = "Amount must be between 1 and 20.")]
        public int AmountInStock { get; set; }

        public int AmountAvailable { get; set; }

        [Required(ErrorMessage = "Please, select Genre.")]
        public int GenreId { get; set; }

        public GenreDto Genre { get; set; }

    }
}