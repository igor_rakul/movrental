﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovRental.Extensions
{
    public static class ExtensionMethods
    {
        public static string ToStringNullableDateTime(this DateTime? dt, string format)
        {
            string str = null;
            if (dt == null)
                return str;
            else
            {
                str = ((DateTime)dt).ToString(format);
                return str;
            }

        }
    }

}