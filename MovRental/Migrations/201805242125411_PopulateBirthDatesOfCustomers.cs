namespace MovRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateBirthDatesOfCustomers : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Customers SET BirthDate = '1981-01-10' WHERE Id = 1");
        }
        
        public override void Down()
        {
        }
    }
}
