namespace MovRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5f60605a-35ab-4f56-b243-a1df3c545ceb', N'guest@vidly.com', 0, N'AJrEARNs41RH+tvpfrint57HtxnQ81HM+RXU3EtINym+4veMEzsWkqIkK1ZEHxlngw==', N'deb3c871-8fb1-4802-8a2c-38babfc51534', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fa90ff4a-1b7f-413a-b8b3-3b6a5f4602ce', N'admin@vidly.com', 0, N'ADVu/oybu65QAHqTwsr/mRbtQ+L2NC0GgqdKDLqFx9yEKnksw/NGsugr6Oq7eUPkIQ==', N'66691a2d-877a-4714-b391-d62a19be2a00', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')

                    INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'b22da081-139e-4792-a747-d5a045cfd9af', N'CanManageMovies')

                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fa90ff4a-1b7f-413a-b8b3-3b6a5f4602ce', N'b22da081-139e-4792-a747-d5a045cfd9af')

                ");

        }
        
        public override void Down()
        {
            Sql(@"

                    DELETE [dbo].[AspNetUsers]
                    WHERE Id IN (N'5f60605a-35ab-4f56-b243-a1df3c545ceb', N'fa90ff4a-1b7f-413a-b8b3-3b6a5f4602ce')

                    DELETE [dbo].[AspNetRoles]
                    WHERE Id IN (N'b22da081-139e-4792-a747-d5a045cfd9af')
            ");
        }
    }
}
