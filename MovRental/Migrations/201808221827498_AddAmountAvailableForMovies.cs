namespace MovRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAmountAvailableForMovies : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "AmountAvailable", c => c.Int(nullable: false));
            Sql("UPDATE Movies SET AmountAvailable = AmountInStock");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Movies", "AmountAvailable");
        }
    }
}
