﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovRental.Models
{


    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}