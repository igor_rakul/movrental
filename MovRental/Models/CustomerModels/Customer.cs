﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MovRental.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please, enter customer's name.")]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }

        public MembershipType MembershipType { get; set; } // - navigation object
        
        // EF recognizes this convention and treats
        // it as a foreign key
        [Display(Name = "Membership Type")]
        [Required(ErrorMessage ="Please, select a membership type")]
        public byte MembershipTypeId { get; set; }

        [Display(Name = "Date of Birth")]
        [Min18YearsIfAMember]
        public DateTime? BirthDate { get; set; }
    }
}