﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovRental.Models
{
    public class MembershipType
    {
        public byte Id { get; set; } // because of a few membership types
        public string Name { get; set; }
        public short SignUpFee { get; set; } // no more than 32k
        public byte DurationInMonths { get; set; } // no more than 12 months
        public byte DiscountRate { get; set; }

        public static readonly byte Unknown = 0;
        public static readonly byte PayAsYouGo = 1;

    }
}