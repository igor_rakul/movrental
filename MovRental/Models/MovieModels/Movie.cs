﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MovRental.Extensions;

namespace MovRental.Models
{
    
    public class Movie
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please, indicate a name of movie.")]
        public string Name { get; set; }
        
        [Display(Name="Date Added")]
        public DateTime? DateAdded { get; set; }
        
        [Display(Name = "Release Date")]
        [Required(ErrorMessage = "Please, indicate a release date.")]
        public DateTime? ReleaseDate { get; set; }

        [Display(Name ="Amount in stock")]
        [Required(ErrorMessage ="Please, indicate an amount in stock.")]
        [Range(1,20, ErrorMessage ="Amount must be between 1 and 20.")]
        public int AmountInStock { get; set; }


        public int AmountAvailable { get; set; }
        
        public Genre Genre { get; set; }

        [Display(Name="Genre")]
        [Required(ErrorMessage ="Please, select Genre.")]
        public int GenreId { get; set; }


        // Extension method for nullable dateTime 
        
    
    }
}