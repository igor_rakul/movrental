﻿This project was made under the course by Mosh Hamedani - ASP.NET MVC 5.

MovRental is a project designed for tracking of movie rented from a shop.
It represents a website having tables with customers, movies, rentals.
It keeps all the records up to date and shows movies, their availability in the shop as well as open/already closed rentals.
Project was completed using ASP.NET MVC, ASP.NET WebAPI, EF.
