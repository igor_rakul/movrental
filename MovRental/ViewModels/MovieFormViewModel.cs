﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MovRental.Models;
using System.ComponentModel.DataAnnotations;

namespace MovRental.ViewModels
{
    public class MovieFormViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }

        public int? Id { get; set; }

        [Required(ErrorMessage = "Please, indicate a name of movie.")]
        public string Name { get; set; }

        [Display(Name = "Date Added")]
        public DateTime? DateAdded { get; set; }

        [Display(Name = "Release Date")]
        [Required(ErrorMessage = "Please, indicate a release date.")]
        public DateTime? ReleaseDate { get; set; }

        [Display(Name = "Amount in stock")]
        [Required(ErrorMessage = "Please, indicate an amount in stock.")]
        [Range(1, 20, ErrorMessage = "Amount must be between 1 and 20.")]
        public int? AmountInStock { get; set; }


        [Display(Name = "Genre")]
        [Required(ErrorMessage = "Please, select Genre.")]
        public int? GenreId { get; set; }

        public MovieFormViewModel()
        {
            Id = 0;
        }
        public MovieFormViewModel(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            ReleaseDate = movie.ReleaseDate;
            AmountInStock = movie.AmountInStock;
            GenreId = movie.GenreId;
            
        }
        public string Title
        {
            get
            {
                return Id == 0 ? "New Movie" : "Edit Movie";
            }
        }
    }
}